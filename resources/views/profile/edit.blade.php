@extends('layouts.base')

@section('content')

<x-app-layout>

    <div class="flex flex-col items-center py-6 bg-jaune">
        <h2 class="font-semibold text-4xl text-black  pb-6 leading-tight">
            {{ __('Espace parent') }}</h2>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 shadow sm:rounded-xl bg-bleu ">
                <div class="max-w-xl">
                    @include('profile.partials.update-profile-information-form')
                </div>
            </div>

            <div class="p-4 sm:p-8  shadow sm:rounded-xl bg-bleu ">
                <div class="max-w-xl">
                    @include('profile.partials.update-password-form')
                </div>
            </div>

            {{-- <div class="p-4 sm:p-8 bg-white shadow sm:rounded-xl ">
                <div class="max-w-xl">
                    @include('profile.partials.delete-user-form')
                </div>
            </div> --}}
        </div>
    </div>
</x-app-layout>

@endsection
@section('body-class', 'bg-jaune')

