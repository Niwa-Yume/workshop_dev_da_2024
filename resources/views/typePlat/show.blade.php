@extends('layouts.base')

@section('content')

<div class="flex justify-center">
    <h1 class="md:text-5xl  font-extrabold my-8 text-white xs:text-2xl">Mes recettes {{ $typeRecipe->name }}es</h1>
</div>

<div class="flex flex-row flex-wrap justify-center overflow-x-auto ">
    @foreach ($typeRecipe->recipes as $recipe)
    <a href="{{ route('recipe.show', $recipe->id) }}" class="m-4 px-10">
        <div class=" rounded-lg p-6 fondcard flex flex-col  ">
            <h2 class="md:text-2xl font-bold mb-2 text-gray-800 text-center xs:text-xl sm:text-xl">{{ $recipe->name }}</h2>
            <img src="{{ $recipe->image }}" alt="Image de {{ $recipe->titre }}"
                class=" h-36 object-cover mb-4 rounded-lg"><p class="text-bleu font-bold">Difficulté :</p>
            <div class="flex gap-20 items-center justify-between">
                @php
                $rating = $recipe->rating;
                if ($rating >= 1 && $rating <= 2.5) { echo ' <p class="purple-star">★</p>' ; } elseif ($rating>=
                    2.6 && $rating <= 3.9) { echo '<p class="purple-star text-5xl">★★</p>' ; } elseif ($rating>= 4 &&
                        $rating <= 5) { echo ' <p class="purple-star ">★★★</p>' ; } @endphp <div
                            class="flex items-center bg-bleu rounded-xl px-1">
                            <img class="w-9 h-9" src="{{ asset('images/formes/alarm.svg') }}"><span
                                class="font-semibold text-white">{{
                                $recipe->temps_prepartion }} min.</span>
            </div>
        </div>




</div>
</a>


@endforeach
@section('title', 'Recettes')

</div>



<style>
    .fondcard {
        background-image: url("https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/post-it%20(1).svg");
        background-size: contain;
        background-repeat: no-repeat;
        /* Évite la répétition du fond */
        background-position: center;
    }

    .purple-star {
        color: #FC5EFF;
        font-size: 30px;
    }
</style>

@endsection

@section('body-class', 'bg-orangeFrigo')