<div class="bg-jaune p-24 flex justify-center items-center">
        <div class="bg-rouge flex flex-col items-center   p-20 px-48 rounded-3xl">
                <img class="w-60" src="{{ asset('images/formes/warning.svg') }}" alt="image d'un warning">
                <p class="text-5xl font-black text-white">Lave toi les mains !</p>
        </div>
</div>