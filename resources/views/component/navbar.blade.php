<div class="flex md:flex-row justify-around items-center text-white xl:pt-4">
    <div class="flex profile-container w-full md:w-auto pl-6 place-content-evenly justify-center gap-20" id="navbar">
        <div class="flex items-center cursor-pointer bg-blue-800 rounded-2xl h-14 pl-2 profile-button place-content-evenly">
            @if(!empty($enfants))
                <a href="#" class="pl-2 flex">
                    <img src="{{ asset($enfants[0]->avatar->fichier) }}" alt="Profile Image"
                        class="h-8 w-8 rounded-full"><svg viewBox="0 0 24 24" class="h-8 w-8 fill-current">
                        <path d="M7 10l5 5 5-5z"></path>
                    </svg></a>
            @else
                <a href="#" class="pl-2 flex">
                    <img src="{{ asset('images/avatars/avatar1.svg') }}" alt="Profile Image"
                        class="h-8 w-8 rounded-full"><svg viewBox="0 0 24 24" class="h-8 w-8 fill-current">
                        <path d="M7 10l5 5 5-5z"></path>
                    </svg>
                </a>
            @endif
        </div>
        <div
            class="profile-details absolute mt-1 bg-sky-900 rounded-lg shadow-lg py-2 transition-opacity duration-300 opacity-0">
            @if(!empty($enfants))
                <a href="{{ route('profile.edit') }}" class="pl-2 flex items-center hover:bg-sky-700 text-white"><img
                        src="{{ asset($enfants[0]->avatar->fichier) }}" alt="Profile Image"
                        class="rounded-full object-contain object-center h-10 w-10 pr-1 text-white">Mon compte</a>
            @endif
            @foreach ($enfants as $enfant)
                <a href="{{ route('enfant.index', $enfant->id) }}"
                    class="flex items-center px-4 py-2 text-sm text-white hover:bg-sky-700">
                    <img src="{{ asset($enfant->avatar->fichier) }}" alt="{{ $enfant->avatar->nom }}"
                        class="object-contain object-center h-10 w-10 pr-1">{{ $enfant->name }}
                </a>
            @endforeach
            @guest
                <a href="{{ route('register') }}"
                    class="block px-4 py-2 text-sm text-white hover:bg-sky-700">Inscription</a>
            @endguest
        </div>

        <!--logo menu hamburger-->
        <a href="{{ route('welcome') }}" class="self-centred">
            <div class="order-first md:order-none self-center w-full md:w-auto text-center md:text-left ">
                <img src="https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/logo.svg" alt="Logo"
                    class="mx-auto md:mx-0 xs:h-24 :w-24 pb-10">
            </div>
        </a>

        <!-- Menu hamburger bouton se connecter -->

        @if(auth()->check())
            <a href="{{ route('logout') }}" class="text-center flex flex-col">
                <img class="h-16" src="{{ asset('images/formes/logout.svg') }}">
                <p class="text-black">Déconnexion</p>
            </a>
        @else
            <a href="{{ route('login') }}" class="text-center flex flex-col">
                <img class="h-16" src="{{ asset('images/formes/login.svg') }}">
                <p class="text-black">connexion</p>
            </a>
        @endif

    </div>
</div>

<!-- Menu hamburger et frites -->


<div class="md:hidden flex align-center justify-center gap-12 mt-5">
    <div class="md:hidden flex align-center justify-center gap-12 ">

        <div class="flex items-center cursor-pointer bg-blue-800 rounded-2xl h-14 pl-2 profile-button">
            @if(!empty($enfants))
                <a href="#" class="pl-2 flex">
                    <img src="{{ asset($enfants[0]->avatar->fichier) }}" alt="Profile Image"
                        class="h-8 w-8 rounded-full"><svg viewBox="0 0 24 24" class="h-8 w-8 text-white fill-current">
                        <path d="M7 10l5 5 5-5z"></path>
                    </svg></a>
            @else
                <a href="#" class="pl-2 flex">
                    <img src="{{ asset('images/avatars/avatar1.svg') }}" alt="Profile Image"
                        class="h-8 w-8 rounded-full"><svg viewBox="0 0 24 24" class="h-8 w-8 fill-current">
                        <path d="M7 10l5 5 5-5z"></path>
                    </svg>
                </a>
            @endif
        </div>
        <div
            class="profile-details absolute mt-1 bg-sky-900 rounded-lg shadow-lg py-2 transition-opacity duration-300 opacity-0">
            @if(!empty($enfants))
                <a href="{{ route('profile.edit') }}" class="pl-2 flex items-center hover:bg-sky-700 text-white"><img
                        src="{{ asset($enfants[0]->avatar->fichier) }}" alt="Profile Image"
                        class="rounded-full object-contain object-center h-10 w-10 pr-1">Mon compte</a>
            @endif
            @foreach ($enfants as $enfant)
                <a href="{{ route('enfant.index', $enfant->id) }}"
                    class="flex items-center px-4 py-2 text-sm text-white hover:bg-sky-700">
                    <img src="{{ asset($enfant->avatar->fichier) }}" alt="{{ $enfant->avatar->nom }}"
                        class="object-contain object-center h-10 w-10 pr-1">{{ $enfant->name }}
                </a>
            @endforeach
            @guest
                <a href="{{ route('register') }}"
                    class="block px-4 py-2 text-sm text-white hover:bg-sky-700">Inscription</a>
            @endguest
        </div>

        <!--logo menu hamburger-->
        <a href="{{ route('welcome') }}" class="self-centred">
            <div class="order-first md:order-none self-center w-full md:w-auto text-center md:text-left ">
                <img src="https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/logo.svg" alt="Logo"
                    class="mx-auto md:mx-0 h-24 w-24 pb-10">
            </div>
        </a>

        <!-- Menu hamburger bouton se connecter -->

        @if(auth()->check())
            <a href="{{ route('logout') }}" class="text-center flex flex-col">
                <img class="h-16" src="{{ asset('images/formes/logout.svg') }}">
                <p class="text-black">Déconnexion</p>
            </a>
        @else
            <a href="{{ route('login') }}" class="text-center flex flex-col">
                <img class="h-16" src="{{ asset('images/formes/login.svg') }}">
                <p class="text-black">connexion</p>
            </a>
        @endif

    </div>

</div>
<script>
    function toggleMenu() {
        const menuDetails = document.querySelector('.profile-details');
        if (menuDetails.style.opacity === '1') {
            menuDetails.style.opacity = '0';
            menuDetails.style.visibility = 'hidden';
            setTimeout(() => { menuDetails.style.display = 'none'; }, 300); // Respect the transition time
        } else {
            menuDetails.style.display = 'block';
            setTimeout(() => {
                menuDetails.style.opacity = '1';
                menuDetails.style.visibility = 'visible';
            }, 10); // Timeout to allow display change to take effect
        }
    }
</script>

<style>
    .profile-button:hover+.profile-details,
    .profile-details:hover {
        opacity: 1;
        visibility: visible;
    }

    .profile-details {
        visibility: hidden;
        transition: visibility 0s linear 1s, opacity 1s linear;
        width: 170px;
    }

    .profile-button,
    .profile-details a {
        font-size: 16px;
        line-height: 24px;
    }

    @media (max-width: 768px) {

        .profile-container,
        .flex.justify-end {
            display: none;
        }

        .hamburger-menu {
            display: block;
        }

        .profile-details {
            visibility: hidden;
            opacity: 0;
            transition: visibility 0s linear 300ms, opacity 300ms linear;
            width: 170px;
            position: absolute;
            background-color: #1a202c;
            z-index: 1000;
        }

        .hamburger-menu:focus+.profile-details,
        .profile-details:hover {
            opacity: 1;
            visibility: visible;
            transition-delay: 0s;
        }

        button {
            margin-bottom: 40px;
        }

        #navbar {
            display: none;
        }


    }

    .hamburger-menu svg {
        fill: #002C9F;
        /* Changez cette valeur pour la couleur désirée, ici blanc */
    }
    .profile-container {
    display: flex;
    flex-direction: row; /* ou column, selon votre disposition */
    justify-content: space-between; /* ajustez ceci selon les besoins */
    width: 100%; /* assurez-vous que le conteneur prend toute la largeur */
    }   

</style>
</head>

</html>