@extends('layouts.base')

@section('content')

<div class="container mx-auto px-4">
    <div class="max-w-2xl mx-auto mt-8">
        <div class="bg-white p-6 rounded-lg shadow">
            <h2 class="text-2xl font-bold mb-4">{{ $enfant->name }}</h2>
            <div class="mb-4">
                <div class="mb-4">
                    <img src="{{ asset($enfant->avatar->fichier) }}" alt="{{ $enfant->avatar->nom }}"
                        class="object-contain object-center h-20 w-20">
                </div>
            </div>
            <p class="text-gray-700 mb-2"><strong>Âge:</strong> {{ $enfant->age }}</p>

            <div class="mt-4">
                <a href="{{ route('enfant.edit', $enfant->id) }}"
                    class="inline-block bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-lg mr-2">Modifier</a>
                <form action="{{ route('enfant.destroy', $enfant->id) }}" method="POST" class="inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit"
                        class="inline-block bg-red-500 hover:bg-red-600 text-white px-4 py-2 rounded-lg">Supprimer</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('title', 'Enfants')
