@extends('layouts.base')

@section('content')
<div class="container mx-auto px-4">
    <p class="text-5xl  font-bold my-8 text-center">Mes enfants</p>

    <div class=" flex flew-row flex-wrap items-center justify-center text-center">
        <!-- Boucle pour afficher les enfants -->
        @foreach($enfants as $key => $enfant)
        <a href="{{ route('welcome') }}" class="m-8">
            <div class="bg-orange shadow-lg  xl:p-24 rounded-full  border border-black xs:p-10">
                <div class="flex flex-col items-center">
                    @if($enfant->avatar)
                    <img src="{{ asset($enfant->avatar->fichier) }}" alt="Avatar de l'enfant"
                        class="xl:w-36 xl:h-36 rounded-full xs:w-20 xs:h-20">
                    @else
                    <span class="w-32 h-32 bg-gray-300 rounded-full flex items-center justify-center text-lg">Image non
                        disponible</span>
                    @endif
                    {{-- <p class="text-sm text-gray-600">Parent: {{ $enfant->user->prenom }}</p>
                    --}}
                </div>
            </div>
            @if ($key == 0)
            <h2 class="mt-2 font-bold text-3xl"><span class="text-white">{{ $enfant->user->prenom_enfant }}
            </h2>
            @else
            <h2 class="mt-2 font-bold text-3xl"><span class="text-white">{{ $enfant->name }}</h2>
            @endif
        </a>

        @endforeach
        <a href="{{ route('enfant.create') }}">
            <div class="bg-blue-800 shadow-lg rounded-full xl:p-32 flex items-center justify-center border border-black xs:p-11">
                <img src="{{ asset('images/formes/plus.svg') }}"class="xs:w-20 xs:h-20">

            </div>
        </a>

    </div>

</div>

@endsection
@section('title', 'Enfants')

@section('body-class', 'bg-sky-600')