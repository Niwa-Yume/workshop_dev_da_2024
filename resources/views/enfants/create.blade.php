<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter un enfant</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100">

<div class="min-h-screen flex items-center justify-center">
    <div class="bg-white p-8 rounded-lg shadow-lg max-w-xl"> <!--  la taille de la card -->
        <form method="POST" action="{{ route('enfant.store') }}">
            @csrf
            <div class="mb-6 border-gray-600"> <!-- la marge entre les éléments -->
                <label for="name" class="block text-sm font-medium text-gray-700 ">Nom enfant</label>
                <input type="text" name="name" id="name" value="" required
                       class="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-600 rounded-md">
            </div>
            <div class="mb-6 border-gray-600"> <!-- la marge entre les éléments -->
                <label for="age" class="block text-sm font-medium text-gray-700 ">Âge</label>
                <input type="number" name="age" id="age" value="" required
                       class="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-600 rounded-md">
            </div>

        <div class="mb-6"> <!--la marge entre les éléments -->
                <label class="block text-sm font-medium text-gray-700">Avatar</label>
                <div class="flex space-x-6"> <!-- l'espace entre les images -->
                        @foreach($avatars as $avatar)
                        <label class="flex items-center">
                                <input type="radio" name="avatar_id" value="{{ $avatar->id }}" required class="focus:ring-blue-500  text-blue-600 border-gray-300"> <!-- Augmentation de la taille de la radio -->
                                <div class="h-16 w-16 flex justify-center items-center  rounded-full border-2 border-gray-300"> <!-- Définition du conteneur pour l'image -->
                                <img src="{{ asset($avatar->fichier) }}" alt="{{ $avatar->nom }}"
                                        class="object-contain object-center h-full w-full"> <!-- Redimensionnement et centrage de l'image -->
                                </div>
                        </label>
                        @endforeach
                </div>
        </div>

            <button type="submit"
                    class="w-full px-4 py-2 bg-blue-500 border border-transparent rounded-md font-semibold text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100">
                Ajouter cet enfant
            </button>
        </form>
    </div>
</div>

</body>
</html>
