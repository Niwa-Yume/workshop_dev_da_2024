<form method="POST" action="{{ route('enfant.update', $enfant->id) }}">
        @csrf
        @method('PUT')
        <div class="mb-6 border-gray-600">
                <label for="name" class="block text-sm font-medium text-gray-700">Nom enfant</label>
                <input type="text" name="name" id="name" value="{{ $enfant->name }}" required
                        class="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-600 rounded-md">
        </div>

        <div class="mb-6 border-gray-600">
                <label for="age" class="block text-sm font-medium text-gray-700">Âge</label>
                <input type="number" name="age" id="age" value="{{ $enfant->age }}" required
                        class="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-600 rounded-md">
        </div>

        <div class="mb-6">
                <label class="block text-sm font-medium text-gray-700">Avatar</label>
                <div class="flex flex-wrap gap-4">
                        @foreach($avatars as $avatar)
                        <label class="flex items-center">
                                <input type="radio" name="avatar_id" value="{{ $avatar->id }}" required
                                        class="focus:ring-blue-500 text-blue-600 border-gray-300" {{ $enfant->avatar_id
                                == $avatar->id ? 'checked' : '' }}>
                                <div
                                        class="h-16 w-16 flex justify-center items-center rounded-full border-2 border-gray-300">
                                        <img src="{{ asset($avatar->fichier) }}" alt="{{ $avatar->nom }}"
                                                class="object-cover object-center h-full w-full rounded-full">
                                </div>
                        </label>
                        @endforeach
                </div>
        </div>


        <button type="submit"
                class="w-full px-4 py-2 bg-blue-500 border border-transparent rounded-md font-semibold text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-gray-100">
                Valider
        </button>
</form>