@extends('layouts.base')

@section('content')
<div class="min-h-100 flex flex-col justify-center items-center p-4">

    @if ($askForCode)
    @section('body-class', 'bg-rouge')
    <div class="felx flex-col">
        <div class="flex items-center justify-center mb-8">
            <img class="w-20 h-20" src="{{ asset('images/formes/warning.svg') }}">
            <p class="text-white xs:text-2xl md::text-4xl font-bold text-center">Appelle un adulte pour cette étape !</p>
        </div>
        <form id="parental_code_form" method="POST"
            class="bg-white p-12 px-36 rounded-3xl flex border border-black xs:p-0 sm:p-0 md:p-3">
            {{ csrf_field() }}
            {{ method_field('PUT')}}
            <div class="flex flex-col gap-6 w-full items-center">
                <div class="flex flex-col items-center">
                    <label for="password" class="text-rouge text-2xl font-extrabold text-center pb-2">Code
                        parental</label>
                    <div class="flex gap-3">
                        <input type="password" name="code_part_1" id="code_part_1"
                            class="bg-rouge rounded-md w-12 text-center text-white text-3xl" maxlength="1"
                            pattern="[0-9]" title="Entrez un chiffre" autofocus/>
                        <input type="password" name="code_part_2" id="code_part_2"
                            class="bg-rouge rounded-md w-12 text-center text-white text-3xl" maxlength="1"
                            pattern="[0-9]" title="Entrez un chiffre" />
                        <input type="password" name="code_part_3" id="code_part_3"
                            class="bg-rouge rounded-md w-12 text-center text-white text-3xl" maxlength="1"
                            pattern="[0-9]" title="Entrez un chiffre" />
                        <input type="password" name="code_part_4" id="code_part_4"
                            class="bg-rouge rounded-md w-12 text-center text-white text-3xl" maxlength="1"
                            pattern="[0-9]" title="Entrez un chiffre" />
                    </div>
                </div>

                <input type="hidden" name="code" id="code" />

                <button class="py-2 w-2/3 bg-bleuNav text-white text-xl font-bold rounded-xl border border-black"
                    type="submit">Valider</button>
            </div>
        </form>
        @if ($etape_precedente)
        <a href="{{ route('etape.show', $etape_precedente->id) }}"
            class="hover:text-blue-600 flex flex-col self-start pt-5 font-bold">
            <img src="{{ asset('images/formes/fleche_precedent.svg') }}" alt="Étape précédente" class="w-24">
            <span class="text-sm">Étape précédente</span>
        </a>
        @else
        <a href="{{ route('recipe.show', $recipe->id) }}" class="hover:text-blue-500 flex flex-col pt-5 font-bold">
            <img src="{{ asset('images/formes/fleche_precedent.svg') }}" alt="Retour à la recette" class="w-24">
            <span class="text-sm">Retour à la recette</span>
        </a>
        @endif

        <script>
            function moveFocus(input, next) {
    var maxLength = parseInt(input.getAttribute('maxlength'));
    if (input.value.length >= maxLength && next) {
        next.focus();
    }
}

document.addEventListener('DOMContentLoaded', function () {
    var inputs = document.querySelectorAll('input[type="password"][name^="code_part_"]');
    
    inputs.forEach(function (input, index) {
        input.addEventListener('keyup', function (event) {
            moveFocus(input, index < inputs.length - 1 ? inputs[index + 1] : null);
        });
    });
});
            document.getElementById('parental_code_form').addEventListener('submit', function() {
            var code = document.getElementById('code_part_1').value +
                       document.getElementById('code_part_2').value +
                       document.getElementById('code_part_3').value +
                       document.getElementById('code_part_4').value;
    
            document.getElementById('code').value = code;
        });
        </script>

    </div>
    @else


    <div
        class="w-full max-w-2xl flex flex-col items-center space-y-6 bg-yellow-300 rounded-lg shadow-lg p-6  border border-black">

        <h1 class="text-4xl font-bold text-center">{{ $recipe->name }} - Étape {{ $etape->ordre }}</h1>

        <video id="myVideo" class="rounded-lg w-full h-auto bg-bleu" controls  autoplay>
            <source src="{{ asset($etape->fichier) }}" type="video/mp4">
            Votre navigateur ne supporte pas les vidéos au format MP4.
        </video>

        <div class="w-full xs:flex-col  md:flex md:flex-row  justify-between items-center xs:space-x-0 xl:space-x-4">
            @if ($etape_precedente)
            <a href="{{ route('etape.show', $etape_precedente->id) }}"
                class="hover:text-blue-600 flex flex-col items-center font-bold">
                <img src="{{ asset('images/formes/fleche_precedent.svg') }}" alt="Étape précédente" class="xs:w-12 xl:w-24">
                <span class="xs:text-xs xl:text-sm">Étape précédente</span>
            </a>
            @else
            <a href="{{ route('recipe.show', $recipe->id) }}"
                class="hover:text-blue-600 flex flex-col items-center font-bold">
                <img src="{{ asset('images/formes/fleche_precedent.svg') }}" alt="Retour à la recette" class="xs:w-12 xl:w-24">
                <span class="xs:text-xs xl:text-sm">Retour à la recette</span>
            </a>
            @endif

            <div class="flex-1 xs:m-0 xl:mx-4 text-center bg-gray-100 xs:p-1 xl:p-4 rounded-lg border border-black">
                <p class="text-lg">{{ $etape->instruction }}</p>
            </div>

            @if ($prochaine_etape)
            <a href="{{ route('etape.show', $prochaine_etape->id) }}"
                class="hover:text-blue-600 flex flex-col items-center font-bold">
                <img src="{{ asset('images/formes/fleche_suivant.svg') }}" alt="Étape suivante" class="xs:w-12 xl:w-24">
                <span class="xs:text-xs xl:text-sm">Étape suivante</span>
            </a>
            @else
            <p class="text-sm">C'est fini</p>
            @endif
        </div>
    </div>
    @endif
</div>

@endsection
@section('title', 'Étapes')

@section('body-class', 'bg-jauneEtape')