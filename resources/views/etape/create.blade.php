@extends('layouts.base')
@section('content')

<form method="POST" action="{{ route('etape.store') }}">
        {{ csrf_field() }}
        <p>
                <label for="titre">Titre étape</label>
                <input type="text" name="titre" id="titre" value="" required />
        </p>
        <p>
                <label for="instruction">Instruction</label>
                <input type="text" name="instruction" id="instruction" value="" required />
        </p>
        <p>
                <label for="ordre"> Ordre</label>
                <input type="text" name="ordre" id="ordre" value="" required />

        </p>
        <p>
                <label for="fichier"> fichier chemin</label>
                <input type="text" name="fichier" id="fichier" value="" required />

        </p>
        <p>
                <label for="recipe_id">Recette</label>
                <select name="recipe_id" id="recipe_id" required>
                        @foreach($recipe_id as $recipe)
                        <option value="{{ $recipe->id }}">
                                {{ $recipe->name }}
                        </option>
                        @endforeach
                </select>
        </p>
        <p>
                <label for="besoin_code_parental">Besoin de code parental</label>
                <input type="checkbox" name="besoin_code_parental" id="besoin_code_parental" value="1">
        </p>







        <button type="submit">Ajouter cette étape</button>
</form>
@endsection