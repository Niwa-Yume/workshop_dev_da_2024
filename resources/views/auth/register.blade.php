<x-guest-layout class="h-screen">
    @php($hidescroll = true)

    <div class="flex flex-col md:flex-row w-full h-screen ">
        <!-- Section de Bienvenue, cachée sur les petits écrans -->
        <div class="hidden md:flex md:w-2/5 p-10 imageDeFond ">
            <a href="{{ route('welcome') }}" class="self-left pr-10">
                <img src="https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/logo.svg" alt="Logo"
                    class="mx-auto md:mx-0">
            </a>
        </div>

        <!-- Section du Formulaire, s'étend sur toute la largeur sur les petits écrans -->
        <div class="w-full md:h-screen flex justify-center items-center border border-black bg-bleuciel imageDeFond2">
            <div
                class="w-full max-w-lg p-10 bg-pink-300 flex flex-col items-center border border-black rounded-none md:rounded-sm">
                <p class="text-white text-3xl mb-4 text-center">Créer un compte</p>

                <!-- Formulaire -->
                <form class="w-full " method="POST" action="{{ route('register') }}">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <!-- Prénom -->
                    <div class="mt-4">
                        <x-input-label class="text-white" for="prenom" :value="__('Nom d\'utilistateur')" />
                        <x-text-input id="prenom" class="block mt-1 w-full h-12 pl-5" type="text" name="prenom"
                            :value="old('prenom')" required autofocus />
                    </div>

                    <!-- Adresse Email -->
                    <div class="mt-4">
                        <x-input-label class="text-white" for="email" :value="__('Email')" />
                        <x-text-input id="email" class="block mt-1 w-full h-12 pl-5" type="email" name="email"
                            :value="old('email')" required autocomplete="username" />
                    </div>

                    <!-- Mot de Passe -->
                    <div class="mt-4">
                        <x-input-label class="text-white" for="password" :value="__('Mot de passe')" />
                        <p class="text-white text-xs">* minimum 10 caractères, 1 chiffre et une majuscule.</p>

                        <x-text-input id="password" class="block mt-1 w-full h-12 pl-5" type="password" name="password"
                            required autocomplete="new-password" />
                    </div>

                    <!-- Confirmation du Mot de Passe -->
                    <div class="mt-4">
                        <x-input-label class="text-white" for="password_confirmation" :value="__('Confirmation mot de passe')" />
                        <x-text-input id="password_confirmation" class="block mt-1 w-full h-12 pl-5" type="password"
                            name="password_confirmation" required autocomplete="new-password" />
                    </div>

                    <!-- Prénom Enfant -->
                    <div class="mt-4">
                        <x-input-label class="text-white" for="prenom_enfant" :value="__('Prénom Enfant')" />
                        <x-text-input id="prenom_enfant" class="block mt-1 w-full h-12 pl-5" type="text" name="prenom_enfant"
                            :value="old('prenom_enfant')" required autofocus />
                    </div>

                    <!-- Code Parental -->
                    <div class="mt-4">
                        <x-input-label class="text-white" for="code_parental"
                            :value="__('Code Parental (4 chiffres)')" />
                        <p class="text-white text-xs">* pour passer les étapes potentielement dangereuse pour l'enfant</p>
                        <x-text-input id="code_parental" class="block mt-1 w-full h-12 pl-5" type="text" name="code_parental"
                            :value="old('code_parental')" required maxlength="4" pattern="\d{4}"
                            title="Veuillez entrer exactement 4 chiffres." />
                    </div>


                    <!-- Bouton d'Inscription -->
                    <div class="flex flex-col items-center justify-center mt-6">
                        <x-primary-button class="w-1/2  h-12 bg-pink-500 rounded-full border border-black">
                            {{ __('S\'enregistrer') }}
                        </x-primary-button>
                    </div>
                </form>

                <!-- Lien vers le formulaire de connexion, ajustement de marge pour réduire l'espace -->
                <a class="underline text-sm text-white-600 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 mt-1"
                    href="{{ route('login') }}">
                    {{ __('Vous avez déjà un compte?') }}
                </a>
            </div>
        </div>
    </div>
    <style>
        .imageDeFond {
            background-image: url("https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/INSCRIPTION%20(2).svg");
            background-size: cover;
            background-repeat: no-repeat;
            background-position: right;
        }

        .imageDeFond2 {
            background-image: url("https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/PATTERN.svg");
            background-size: contain;
            background-position: left;
        }
    </style>
</x-guest-layout>
@section('body-class', 'bg-pink-500')
@section('title', 'Inscription')