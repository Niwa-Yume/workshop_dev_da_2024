<x-guest-layout>
    @php($hidescroll = true)
    @section('title', 'Connexion')

    <div class="flex min-h-screen xs:flex-col md:flex-wor xl:flex-row xs:w-full  md:w-full xl:w-full ">
        <!-- Div de Texte -->
        <div class="md:w-1/2 xl:w-1/2  flex xs:px-2 md:p-10 bg-bleuClair flex-col items-start imageDeFond xs:order-2 md:order-1 grow">
            <a href="{{ route('welcome') }}" class="self-left xs:pr-0 md:pr-10 xl:pr-10 flex flex-col items-end">
                <img src="https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/logo.svg" alt="Logo"
                    class="mx-auto md:mx-0 xs:h-20 xs:w-20 sm:h-24 sm:w-24 md:w-36 mh:h-36"></a>
        </div>

        <!-- Formulaire de Connexion -->
        <div class="md:w-1/2 xl:w-1/2  flex justify-center items-center border border-black imageDeFond2 bg-bleuciel xs:order-1">
            <div class="xs:my-7 xl:w-full max-w-md bg-bleu rounded-3xl p-8 border border-black">
                <p class=" text-white xs:text-2xl sm:text-xl xl:text-3xl font-bold text-center mb-5">Se connecter</p>
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <!-- Email Address -->
                    <div>
                        <x-input-label class="text-white xs:text-base sm:text-base xl:text-xl" for="email" :value="__('Email')" />
                        <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus autocomplete="username" />
                        <x-input-error :messages="$errors->get('email')" class="mt-2" />
                    </div>

                    <!-- Password -->
                    <div class="mt-4">
                        <x-input-label class="text-white xs:text-base sm:text-base xl:text-xl" for="password" :value="__('Mot de passe')" />
                        <x-text-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
                        <x-input-error :messages="$errors->get('password')" class="mt-2" />
                    </div>

                    <!-- Remember Me -->
                    <div class="block mt-4 xs:mb-2">
                        <label for="remember_me" class="inline-flex items-center">
                            <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:ring-indigo-500" name="remember">
                            <span class="ms-2 text-sm text-white">{{ __('Se souvenir de moi') }}</span>
                        </label>
                    </div>

                    <div class="flex flex-col items-center justify-center">
                        <!-- Validation Button -->
                        <x-primary-button class="w-1/2  h-10 bg-pink-500 rounded-full border border-black">
                            {{ __('Se connecter') }}
                        </x-primary-button>

                        {{-- <div class="flex items-center justify-end mt-4">
                            @if (Route::has('password.request'))
                                <a class="underline text-sm text-white " href="{{ route('password.request') }}">
                                    {{ __('Mot de passe oublié ?') }}
                                </a>
                            @endif
                        </div> --}}

                        <!-- Créer un compte -->
                        <div class="flex items-center justify-end mt-4">
                            <a class="underline text-sm text-white  rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-900" href="{{ route('register') }}">
                                {{ __('Créer un compte') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-guest-layout>
<style>
    .imageDeFond{
        background-image: url("https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/INSCRIPTION%20(2).svg");
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        
    }
    .imageDeFond2{
        background-image: url("https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/PATTERN.svg");
        background-size: contain;
        
        background-position: left;
    }

    @media (max-width: 540px) {
        .imageDeFond{
            background-image: url('/images/formes/bg-login-mobile.svg');
            background-size: cover;
            background-position: bottom;

        }}

</style>
