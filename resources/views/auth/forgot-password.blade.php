<x-guest-layout>
    @php($hidescroll = true)

    <div class="max-w-lg mx-auto bg-slate-200 shadow-lg rounded-lg p-8">
        <h2 class="text-2xl font-semibold text-center text-black mb-6">
            {{ __('Mot de Passe Oublié ?') }}
        </h2>
        <div class="mb-4 text-sm text-gray-600 text-center">
            {{ __('Tu as oublié ton mot de passe ? Aucun sousci ! nous allons t\'envoyer un mail aec un lien pour réinistaliser ton mot de passe.') }}
        </div>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4 text-center" :status="session('status')" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <!-- Adresse Email -->
            <div class="mb-6">
                <x-input-label for="email" :value="__('Email')" class="text-black" />
                <x-text-input id="email" class="block mt-2 w-full rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500" type="email" name="email" :value="old('email')" required autofocus />
                <x-input-error :messages="$errors->get('email')" class="mt-2 text-red-600" />
            </div>

            <!-- Bouton d'envoi -->
            <div class="flex items-center justify-center mt-4">
                <x-primary-button class="bg-blue-600 hover:bg-blue-700 text-white px-6 py-2 rounded-lg">
                    {{ __('Le liens pour son nouveau mot de passe') }}
                </x-primary-button>
            </div>
        </form>
    </div>
</x-guest-layout>
