@extends('layouts.base')

@section('content')
<style>
    .container {
        max-width: 600px;
        margin: 0 auto;
        padding: 20px;
    }

    /* espace entre les composant */
    .form, .button-container, .descr-container {
        margin-bottom: 20px; /* ajuster selon besoni */
    }

    .form {
        position: relative;
        display: flex;
        flex-direction: column;
        gap: 10px;
        width: 100%; /* Ajuster taille pour le container */
        background-color: white;
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0 30px 30px -30px rgba(27, 26, 26, 0.315);
    }

    .form .title {
        color: royalblue;
        font-size: 30px;
        font-weight: 600;
        letter-spacing: -1px;
        line-height: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 20px; /* espace pour le titre */
    }

    .form input, .form textarea {
        outline: none;
        border: 1px solid rgb(219, 213, 213);
        padding: 8px 14px;
        border-radius: 8px;
        width: 100%;
        margin-bottom: 10px; /* espace pour les champs */
    }

    .form textarea {
        height: 100px;
        resize: none;
    }

    .form button {
        align-self: flex-end;
        padding: 8px;
        border: none;
        border-radius: 8px;
        font-size: 16px;
        font-weight: 500;
        background-color: royalblue;
        color: white;
        cursor: pointer;
    }

</style>

<div class="container">
    <form class="form">
        <div class="title">Contactez-nous</div>
        <input type="text" placeholder="ton email" class="input">
        <textarea placeholder="Votre message"></textarea>
        <button class="flex items-center bg-blue-500 text-white gap-1 px-4 py-2 cursor-pointer text-gray-800 font-semibold tracking-widest rounded-md hover:bg-blue-400 duration-300 hover:gap-2 hover:translate-x-3">
            Envoyer
            <svg class="w-5 h-5" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 12 3.269 3.125A59.769 59.769 0 0 1 21.485 12 59.768 59.768 0 0 1 3.27 20.875L5.999 12Zm0 0h7.5" stroke-linejoin="round" stroke-linecap="round"></path>
            </svg>
        </button>
    </form>
</div>

@endsection
