@extends('layouts.base')

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page de contact</title>
    <style>
        .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            gap: 30px; /* Espace entre les formulaires */
        }

        .form {
            display: flex;
            align-items: center;
            flex-direction: column;
            gap: 20px; /* Espacement entre les champs du formulaire */
            width: 300px;
            background-color: white;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 30px 30px -30px rgba(27, 26, 26, 0.315);
        }

        .form .title {
            color: royalblue;
            font-size: 30px;
            font-weight: 600;
            letter-spacing: -1px;
            line-height: 30px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .form input, .form textarea {
            outline: 0;
            border: 1px solid rgb(219, 213, 213);
            padding: 8px 14px;
            border-radius: 8px;
            width: 100%;
            box-sizing: border-box;
        }

        .form input {
            height: 50px;
        }

        .form textarea {
            height: 100px;
            resize: none;
        }

        .form button {
            align-self: flex-end;
            padding: 8px;
            outline: 0;
            border: 0;
            border-radius: 8px;
            font-size: 16px;
            font-weight: 500;
            background-color: royalblue;
            color: #fff;
            cursor: pointer;
        }

        .button-container {
            display: flex;
            gap: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Contactez-nous</h1>
        <form class="form">
            <div class="title">Formulaire de contact</div>
            <input type="text" placeholder="Votre email" class="input">
            <textarea placeholder="Votre message"></textarea>
            <div class="button-container">
                <button type="submit">Envoyer</button>
                <button type="reset">Réinitialiser</button>
            </div>
        </form>
    </div>
    <div>
    <!-- Bouton -->
    <div style="position: fixed; right: 30px; bottom: 30px;">
        <a href="/chemin-vers-la-prochaine-page" class="bg-violet-700 hover:bg-violet-950 text-white font-bold py-2 px-4 rounded-full">
            Continuer
        </a>
    </div>
    <!--bouton avec design-->
    <button class="flex items-center bg-blue-500 text-white gap-1 px-4 py-2 cursor-pointer text-gray-800 font-semibold tracking-widest rounded-md hover:bg-blue-400 duration-300 hover:gap-2 hover:translate-x-3">
        Continuer
        <svg class="w-5 h-5" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M6 12 3.269 3.125A59.769 59.769 0 0 1 21.485 12 59.768 59.768 0 0 1 3.27 20.875L5.999 12Zm0 0h7.5"stroke-linejoin="round" stroke-linecap="round"></path>
        </svg>
    </button>
    <!--bouton avec design-->
    <button class="bg-cyan-950 text-cyan-400 border border-cyan-400 border-b-4 font-medium overflow-hidden relative px-4 py-2 rounded-md hover:brightness-150 hover:border-t-4 hover:border-b active:opacity-75 outline-none duration-300 group">
        <span class="bg-cyan-400 shadow-cyan-400 absolute -top-[150%] left-0 inline-flex w-80 h-[5px] rounded-md opacity-50 group-hover:top-[150%] duration-500 shadow-[0_0_10px_10px_rgba(0,0,0,0.3)]"></span>
        Mets ton curseur sur moi
    </button>


<!--tentative de card pour les plats-->
<div class="max-w-sm rounded-lg overflow-hidden shadow-xl m-4 bg-white">
        <img class="w-full h-48 object-contain" src="https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/oeuf-style-cartoon_1308-111705.avif" alt="Recipe Image">
        <div class="px-6 py-4 text-center">
            <div class="font-bold text-xl mb-2">Recipe Title</div>
            <p class="text-gray-700 text-base">Delicious and easy to prepare...</p>
        </div>
        <div class="flex justify-center items-center px-6 pt-4 pb-2">
            <img src="https://img.icons8.com/emoji/48/star-emoji.png" alt="star-emoji" class="w-8 h-8"/>
            <img src="https://img.icons8.com/emoji/48/star-emoji.png" alt="star-emoji" class="w-8 h-8"/>
            <img src="https://img.icons8.com/emoji/48/star-emoji.png" alt="star-emoji" class="w-8 h-8"/>
            <img src="https://img.icons8.com/emoji/48/star-emoji.png" alt="star-emoji" class="w-8 h-8"/>
            <img src="https://img.icons8.com/emoji/48/star-emoji.png" alt="star-emoji" class="w-8 h-8"/>
            <span class="ml-2 text-sm">30 minutes</span>
        </div>
</div>


















</body>
</html>
@endsection
