@extends('layouts.base')
@php($hidescroll = true)

@section('content')
<div class="flex flex-col md:flex-row min-h-screen w-full  xs:justify-center md:justify-start xs:w-3/4  sm:w-3/4" id="main-content">
    <div
        class="flex flex-col xs:flex-col  sm:items-center xs:items-center sm:flex-col xl:flex-row xxl:w-full  xs:px-0 xs:m-0 md:px-0 xl:px-4">
        <div class="flex flex-col items-left pr-48 md:p-4">
            <a href="{{ route('type_recipe.show', 1) }}">
                <img class=" max-w-xs md:max-w-none xs:ml-0 md:m-0 md:mt-72 xs:w-36 xl:ml-0 xl:w-2/4" src="{{ asset('images/formes/sucrediv.svg') }}"
                    alt="Sweet Recipes">
            </a>
        </div>
        <div class="flex flex-col p-2 md:p-4 items-left pr-52">
            <a href="{{ route('type_recipe.show', 2) }}">
                <img class=" max-w-xs md:max-w-none xs:ml-0 md:m-0 xs:w-36 xl:ml-0 xl:w-2/4" src="{{ asset('images/formes/saleediv.svg') }}"
                    alt="Savory Recipes">
            </a>
        </div>
    </div>
</div>

@endsection

@section('body-class', 'bg-jaune')
@section('background-image')
@section('title', 'Accueil')

<style>
    body {
        background-image: url('/images/formes/bg-home.svg');
        background-size: cover;
        background-position: center;

        background-repeat: no-repeat;

    } 

    /* Tailles réduites pour les téléphones */
    @media (max-width: 540px) {
        body {
            background-image: url('/images/formes/bg-home.svg');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;

        }
    }

    /* Tailles réduites pour les tablettes test */
    @media (max-width: 1024px) {
        body {
            background-size: 175% auto;
        }
    }
</style>
@endsection  

