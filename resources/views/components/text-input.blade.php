@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => ' rounded-full border border-black focus:border-indigo-500 focus:indigo-500  shadow-sm']) !!}>
