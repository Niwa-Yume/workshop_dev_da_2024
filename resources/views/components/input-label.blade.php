@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-bold text-md text-white']) }}>
    {{ $value ?? $slot }}
</label>
