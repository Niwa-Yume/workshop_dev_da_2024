@extends('layouts.base')

@section('content')

<div class="flex justify-center">
    <h1 class="text-3xl font-bold my-8 text-white">Nos recettes sucrée ou salées</h1>
</div>

<div class="flex flex-row flex-wrap justify-center overflow-x-auto">
    @foreach ($recipes as $recipe)
    <a href="{{ route('recipe.show', $recipe->id) }}" class="m-4">
        <div class=" rounded-lg p-6 fondcard" style="min-width: 300px;">
            <h2 class="text-2xl font-bold mb-2 text-gray-800 text-center">{{ $recipe->name }}</h2>
            <img src="{{ $recipe->image }}" alt="Image de {{ $recipe->titre }}" class="w-full h-48 object-cover mb-4 rounded-lg">
            <div class="  h-36 flex gap-20 items-center">
                @php
                $rating = $recipe->rating;
                if ($rating >= 1 && $rating <= 2.5) { echo ' <p class="purple-star">★</p>' ; }
                elseif ($rating>=
                    2.6 && $rating <= 3.9) {
                        echo '<p class="purple-star text-5xl">★★</p>' ; }
                elseif
                        ($rating>= 4 && $rating <= 5) {
                            echo ' <p class="purple-star ">★★★</p>'
                            ; }
                @endphp
                <div class="flex items-center bg-bleu rounded-xl px-1">
                    <img class="w-9 h-9" src="{{ asset('images/formes/alarm.svg') }}"><span
                        class="font-semibold text-white">{{
                            $recipe->temps_prepartion }} min.</span>
                </div>
            </div>
        </div>
    </a>
@endforeach
</div>

{{-- <div class="flex justify-center mt-4">
    <div class="bg-blue-200 w-52 rounded-md text-center py-1 font-bold">
        <a href="{{ route('recipe.create') }}">
            {{ __('Ajouter une recette') }}
        </a>
    </div>
</div> --}}
<style>
    .fondcard{
        background-image: url("https://raw.githubusercontent.com/Niwa-Yume/image-worshop/main/post-it%20(1).svg");
        background-size: contain;
        background-repeat: no-repeat; /* Évite la répétition du fond */
        background-position: center;
    }
    
</style>
@endsection
@section('body-class', 'bg-sky-600')