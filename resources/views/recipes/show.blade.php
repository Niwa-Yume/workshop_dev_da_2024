@extends('layouts.base')

@section('content')

<p class="md:text-3xl xl:text-4xl font-bold my-8 text-center mt-60 xs:text-3xl xs:mt-36 sm:mt-36 md:mt-32 xl:mt-60">Ma
    liste de courses :</p>
<div
    class="flex  items-center justify-center gap-8  xs:flex-col sm:flex-col md:flex-row lg:flex-row xl:flex-row xxl:flex-row xxxl:flex-row xl:pb-10">
    <a href="{{ route('type_recipe.show', $recipe->typeRecipe->id) }}"
        class="hover:text-blue-600 flex flex-col  font-bold self-end xs:self-center sm:self-center md:self-end xs:order-3 sm:order-3 md:order-1 xl:order-1"><img
            class="w-24" src="{{ asset('images/formes/fleche_precedent.svg') }}"><span class="text-sm">Revenir au
            {{$recipe->typeRecipe->name}}</span>
    </a>
    <!-- Section des ingrédients -->
    <div class="xs:w-10/12	 max-w-md  bg-cover flex flex-col xs:order-1"
        style="background-image: url('/images/formes/liste.svg');">
        <p class="xs:text-xl xl:text-2xl font-bold  text-center mt-28 mb-4">Ingrédients :</p>

        <!-- Boucle pour afficher les ingrédients -->
        <div class="flex flex-col">
            @foreach($recipe->ingredientRecipes as $ingredient)
            <div class="flex items-center  justify-between  mb-2 ml-4">
                <div class="flex items-center"><input type="checkbox" name="ingredients[]" value="{{ $ingredient->id }}" class="mr-2  rounded-sm  w-6 h-6">
                    <span class="xs:font-medium xl:text-xl">{{ $ingredient->productIngredient->name }} {{ $ingredient->quantity }} {{
                        $ingredient->uniteIngredient->name }}</span>
                </div>
                <img class="h-16 mr-3" src="{{ $ingredient->productIngredient->image }}"
                    alt="{{ $ingredient->productIngredient->name }}">

            </div>

            @endforeach
        </div>
    </div>


    <!-- Section des ustensiles -->
    <div class="w-10/12	 max-w-md  bg-cover min-h-96 flex flex-col xs:order-2"
        style="background-image: url('/images/formes/liste.svg');">
        <p class="xs:text-xl xl:text-2xl font-bold  text-center mt-28  ">Ustensiles :</p>

        <!-- Boucle pour afficher les ustensiles -->
        @foreach($recipe->liaison_ustensils_recipes as $liaison)
        <div class="flex items-center  justify-between  mb-2 ml-4">
            <div class="flex items-center"><input type="checkbox" name="ustensils[]" value="{{ $liaison->ustensil->id }}" class="mr-2 rounded-sm w-6 h-6">
                <span class="xs:font-medium xl:text-xl">{{ $liaison->ustensil->name }}</span>
            </div>
            <img class="h-16 rounded-md  mr-3" src="{{ $liaison->ustensil->image }}"
                alt="{{ $liaison->ustensil->name }}">
        </div>
        @endforeach
    </div>
    @if ($premiere_etape)
    <a href="{{ route('etape.show', $premiere_etape->id) }}"
        class="hover:text-blue-600 flex flex-col  font-bold self-end xs:order-4 xs:self-center sm:self-center md:self-end"><img
            class="w-24" src="{{ asset('images/formes/fleche_suivant.svg') }}"><span class="text-sm">Étape
            suivante</span>
    </a>
    @else
    <p>On ne peut pas commencer!!!</p>
    @endif
</div>



@endsection
@section('body-class', 'bg-newJaune')
@section('background-image')
@section('title', 'Recettes')
<style>
    body {
        background-image: url('/images/formes/bg-liste.svg');
        background-size: cover;
        background-repeat: no-repeat;

    }

    @media (max-width: 800px) {
        body {
            background-image: url('/images/formes/bg-liste-mobile.svg');
        }
    }

    @media (max-width: 460px) {
        body {
            background-image: url('/images/formes/bg-liste-mobile-sans-bonhomme.svg');
        }
    }
</style>
@endsection