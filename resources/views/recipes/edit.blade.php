<form method="POST" action="{{ route('recipe.update', $recipe->id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <p>
                <label for="titre">{{ __('Titre') }}</label>
                <input type="text" name="titre" id="titre" value="{{ $recipe->titre }}" required />
        </p>
        <p>
                <label for="description">{{ __('Description') }}</label>
                <input type="text" name="description" id="description" value="{{ $recipe->description }}" required />
        </p>
        <p>
                <label for="difficulte">{{ __('Difficulte') }}</label>
                <input type="text" name="difficulte" id="difficulte" value="{{ $recipe->difficulte }}" required />
        </p>

        <p>
                <label for="ingredients">{{ __('Ingredients') }}</label>
                <input type="text" name="ingredients" id="ingredients" value="{{ $recipe->ingredients }}" required />
        </p>

        <p>
                <label for="ustensils">{{ __('Ustensils') }}</label>
                <input type="text" name="ustensils" id="ustensils" value="{{ $recipe->ustensils }}" required />
        </p>
        <p>
                <label for="temps_preparation">{{ __('Temps_preparation') }}</label>
                <input type="text" name="temps_preparation" id="temps_preparation" value="{{ $recipe->temps_preparation }}" required />
        </p>
        <p>
                <label for="image">{{ __('Image') }}</label>
                <input type="text" name="image" id="image" value="{{ $recipe->image }}" required />
        </p>

        <p>
                <label>{{ __('Plat sucré') }}</label>
                <input type="radio" name="plat_sucre" id="plat_sucre_oui" value="oui" {{ $recipe->plat_sucre === 'oui' ? 'checked' : '' }}>
                <label for="plat_sucre_oui">{{ __('Oui') }}</label>
        
                <input type="radio" name="plat_sucre" id="plat_sucre_non" value="non" {{ $recipe->plat_sucre === 'non' ? 'checked' : '' }}>
                <label for="plat_sucre_non">{{ __('Non') }}</label>
            </p>
        
       
        <button type="submit">Valider</button>
</form>