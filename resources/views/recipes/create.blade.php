<form method="POST" action="{{ route('recipe.store') }}">
        {{ csrf_field() }}
        <p>
                <label for="name">Nom recette</label>
                <input type="text" name="name" id="name" value="" required />
        </p>
        <p>
                <label for="description">Description</label>
                <input type="text" name="description" id="description" value="" required />
        </p>
        <p>
                <label for="image">image URL</label>
                <input type="text" name="image" id="image" value="" required />

        </p>
        <p>
                <label for="types_recipes_id">Type de plat</label>
                <select name="types_recipes_id" id="types_recipes_id" required>
                        @foreach($types_recipes as $type)
                        <option value="{{ $type->id }}">
                                {{ $type->name }}
                        </option>
                        @endforeach
                </select>
        </p>

        <p>
                <label for="rating">Rating (max 5)</label>
                <select name="rating" id="rating" required>
                        @for($i = 0.5; $i <= 5; $i +=0.5) <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                </select>
        </p>
        <p>
                <label for="temps_prepartion">Temps de préparation  </label>
                <input type="text" name="temps_prepartion" id="temps_prepartion" value="" required />

        </p>




        <button type="submit">Ajouter la recette</button>
</form>