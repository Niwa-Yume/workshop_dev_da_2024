import defaultTheme from "tailwindcss/defaultTheme";
import forms from "@tailwindcss/forms";

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
        "./storage/framework/views/*.php",
        "./resources/views/**/*.blade.php",
    ],

    theme: {
        extend: {
            screens: {
                xs: "300px", // Exemple d'ajout d'un nouveau point de rupture
                sm: "640px", // Tailwind's default
                md: "768px",
                lg: "1024px",
                xl: "1280px",
                xxl: "1536px",
                xxxl: "1920px", // Exemple d'ajout d'un point de rupture supplémentaire
            },

            colors: {
                orange: "#FF5c02", // Ajoutez votre couleur personnalisée ici
                orangeClair: "#FF9930", // Ajoutez votre couleur personnalisée ici
                orangeTresClair: "#FFA800", // Ajoutez votre couleur personnalisée ici
                jaune: "#ffd833",
                newJaune:"#f9d833",
                jauneEtape: "#eab307",
                bleu: "#4A53FE",
                rouge: "#FF0000",
                bleufond: "#0094ff",
                bleuNav: "#002C9F",
                roseBonbon: "#F3bbE3",
                Rose: "#F363D3",
                bleuciel: "#C9E4F7",
                vert: "#8BC55E",
                gris: "#8386A5",
                fondRecette: "#9DC0D9",
                violet:"#FC5EFF",
                orangeFrigo:"#FFA800",
                // Extension des points de rupture
            },
        },

        plugins: [forms],
    },
};
