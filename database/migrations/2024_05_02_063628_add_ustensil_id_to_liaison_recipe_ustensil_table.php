<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('liaison_recipe_ustensils', function (Blueprint $table) {
            //
            $table->foreignId('ustensil_id')
                 ->constrained('ustensils');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('liaison_recipe_ustensils', function (Blueprint $table) {
            //
            $table->dropForeign(['ustensil_id']);
        	$table->dropColumn(['ustensil_id']);
        });
    }
};
