<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\ControllerListe;
use App\Http\Controllers\DesignSystemController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\TypeRecipeController;

use App\Http\Controllers\EnfantController;
use App\Http\Controllers\EtapeController;


use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/dashboard', function () {
    return redirect()->route('profile.edit');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::resource('type_recipe', TypeRecipeController::class);


Route::resource('recipe', RecipeController::class);


Route::resource('designsystem', DesignSystemController::class);

Route::resource('contact', ContactController::class);





Route::middleware('auth')->group(function () {
    Route::resource('enfant', EnfantController::class);
    Route::resource('etape', EtapeController::class);
    //Route::post('/etape/{id}', [EtapeController::class, 'show']);

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
