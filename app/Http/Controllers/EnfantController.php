<?php

namespace App\Http\Controllers;

use App\Models\Enfant;
use Illuminate\Support\Facades\Auth;
use App\Models\Avatar;

use Illuminate\Http\Request;
use App\Http\Requests\EnfantRequest;


class EnfantController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

        $user = auth()->user(); // Récupérer l'utilisateur connecté
        $enfants = $user->enfants()->paginate(5); // Récupérer les enfants du user connecté
        return view('enfants.index', compact('enfants'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $avatars = Avatar::all(); // Récupérer tous les avatars
        return view('enfants.create', compact('avatars'));

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EnfantRequest $request)
    {
        //
        $user = Auth::user(); // Récupérer l'utilisateur connecté

        // Créer un nouvel enfant avec les données validées du formulaire et l'ID de l'utilisateur connecté
        $enfant = new Enfant($request->validated());
        $enfant->user_id = $user->id;
        $enfant->save();

        return redirect()->route('enfant.index')->with('ok', __('Enfant has been saved'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $avatars = Avatar::all(); // Récupérer tous les avatars

        $enfant = Enfant::findOrFail($id);
        return view('enfants.show', ['enfant' => $enfant,'avatars' => $avatars]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Enfant $enfant)
    {
        $avatars = Avatar::all(); // Récupérer tous les avatars
        return view('enfants.edit', ['enfant' => $enfant, 'avatars' => $avatars]);

        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EnfantRequest $request, Enfant $enfant)
    {
        //
        $enfant->update($request->all());
        return redirect()->route('enfant.index')
            ->with('ok', __('Enfant has been updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Enfant $enfant)
    {
        //
        $enfant->delete();
        return response()->json();
    }
}
