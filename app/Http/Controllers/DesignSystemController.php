<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DesignSystemController extends Controller
{
    // This is the index method
    public function index()
    {
        return view('designsystem.index');
    }
}
