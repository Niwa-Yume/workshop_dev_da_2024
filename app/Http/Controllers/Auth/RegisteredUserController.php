<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            //'name' => [''],
            'prenom' => ['required', 'string', 'max:255'], // Nouvelle validation pour le prénom
            
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', 'min:10'],
            'prenom_enfant' => ['required', 'string',  'max:55'],

            'code_parental' => ['required', 'string', 'max:255'], // Nouvelle validation pour le code parental
        ]);

        $user = User::create([
            //'name' => $request->name,
            'prenom' => $request->prenom, // Ajout du champ prénom
            
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'prenom_enfant' => $request->prenom_enfant,

            'code_parental' => $request->code_parental, // Ajout du champ code parental
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(route('enfant.index', absolute: false));
    }
}
