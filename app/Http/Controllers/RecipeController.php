<?php

namespace App\Http\Controllers;
use App\Models\Recipe;

use App\Models\TypeRecipe;

use Illuminate\Http\Request;
use App\Http\Requests\RecipeRequest;


class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $recipes =  Recipe::paginate(5);

        return view('recipes.index', ['recipes' => $recipes]);
         
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('recipes.create',['types_recipes' => TypeRecipe::all()]);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RecipeRequest $request)
    {

        Recipe::create($request->validated());


        return redirect()->route('recipe.index')
        ->with('ok', __('Recipe has been saved'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $recipe = Recipe::findOrFail($id);
        $etapes = \DB::table('etapes')
            ->where('recipe_id','=',$recipe->id)
            ->orderBy('ordre', 'asc')
            ->limit(1)
            ->get()
        ;
        $premiere_etape = $etapes[0] ?? null;

        return view('recipes.show', ['recipe' => $recipe, 'premiere_etape' => $premiere_etape]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Recipe $recipe)
    {
        return view('recipes.edit', ['recipe' => $recipe]);
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RecipeRequest $request, Recipe $recipe)
    {
        $recipe->update( $request->all() );   
        return redirect()->route('recipe.index')
                         ->with( 'ok', __('Recipe has been updated') );
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Recipe $recipe)
    {
       $recipe->delete();
       return response()->json();
    }

}
