<?php

namespace App\Http\Controllers;
use App\Models\Recipe;
use App\Http\Requests\EtapeRequest;

use App\Models\Etape;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EtapeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        throw new NotFoundHttpException();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('etape.create',['recipe_id' => Recipe::all()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EtapeRequest $request)
    {
        //
        Etape::create($request->validated());


        return redirect()->route('recipe.index')
        ->with('ok', __('etape has been saved'));
    }


    private function showEtapePage(Etape $etape, $codeByUser){

        //
        $recipe = $etape->etapeRecipe;

        $etape_precedente = Etape::where('recipe_id', '=', $recipe->id)
                              ->where('ordre', '<', $etape->ordre)
                              ->orderByDesc('ordre')
                              ->first();


        $askForCode = $etape->besoin_code_parental;
        if($askForCode){

            if($codeByUser){

                $user = auth()->user();

                if($codeByUser == $user->code_parental){
                    $askForCode = false;
                }
            }

        }

        $prochaine_etapes = \DB::table('etapes')
            ->where('recipe_id','=',$recipe->id)
            ->where('ordre', '>', $etape->ordre)
            ->orderBy('ordre', 'asc')
            ->limit(1)
            ->get()
        ;

        $prochaine_etape = $prochaine_etapes[0] ?? null;


        return view('etape.show', [
            'recipe' => $recipe,
            'etape' => $etape,   
            'askForCode' => $askForCode,
            'etape_precedente' => $etape_precedente,
            'prochaine_etape' => $prochaine_etape
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Etape $etape)
    {
        return $this->showEtapePage($etape,'');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Etape $etape)
    {
        return $this->showEtapePage($etape, $request->request->get('code'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
