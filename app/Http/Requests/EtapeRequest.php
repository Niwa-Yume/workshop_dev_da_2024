<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EtapeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //
            'titre' => '', // 'required|string|max:20',
            'instruction' => '', // 'required|string|max:100',
            'ordre' => '', // 'string|max:200',
            'recipe_id' => '', // 'required|integer|max:20000',
            'fichier' => '', // 'required|integer|max:20000',

            'besoin_code_parental' => '', // 'required|integer|max:20000',
        ];
    }
}
