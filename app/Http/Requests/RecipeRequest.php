<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //
            'name' => '', // 'required|string|max:20',
            'description' => '', // 'required|string|max:100',
            'image' => '', // 'string|max:200',
            'types_recipes_id' => '', // 'required|integer|max:20000',

            'rating' => '', // 'required|string|max:10',
            'temps_prepartion' => '', // 'required|integer|max:20000',

        ];
    }
}
