<?php

namespace App\View\Components;


use Illuminate\View\Component;
use Illuminate\View\View;
use App\Models\Enfant;


class Navbar extends Component
{
        /**
         * Create the component instance.
         */
        public function __construct(
                //public string $type,
                //public string $message,
        ) {
        }


        /**
         * Get the view / contents that represent the component.
         */
        public function render(): View
        {
                $enfants = [];

                // Vérifier si l'utilisateur est authentifié
                if (auth()->check()) {
                        $user = auth()->user();
                        // Récupérer les enfants de l'utilisateur s'il est authentifié
                        $enfants = $user->enfants()->get();
                }

                return view('component.navbar', [
                        'enfants' => $enfants
                ]);
        }
}
