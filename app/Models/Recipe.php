<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;


    protected $table = 'recipes';

    protected $fillable = [
        'name','description','image', 'rating','temps_prepartion','types_recipes_id'
    ];

    public function typeRecipe(){
        return $this->belongsTo(TypeRecipe::class, 'types_recipes_id');
    }


    public function liaison_ustensils_recipes(){
        return $this->hasMany(Liaison_recipe_ustensil::class);
    }
    public function ingredientRecipes(){
        return $this->hasMany(Ingredient::class);
    }

    public function recipesEtapes(){
        return $this->hasMany(Etape::class);
    }
}
