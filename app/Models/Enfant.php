<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enfant extends Model
{
    use HasFactory;


    protected $table = 'enfants';

    protected $fillable = [
        'name','profile_image','age','avatar_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function avatar(){
        return $this->belongsTo(Avatar::class);
    }
}
