<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeRecipe extends Model
{
    use HasFactory;

    protected $table = 'types_recipes';

    protected $fillable = [
        'name'
    ];


    public function recipes(){
        return $this->hasMany(Recipe::class, 'types_recipes_id');
    }
}
