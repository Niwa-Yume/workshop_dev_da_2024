<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    use HasFactory;

    protected $fillable = [
        'fichier'
    ];

    public function enfants()
    {
        return $this->hasMany(Enfant::class);
    }
}
