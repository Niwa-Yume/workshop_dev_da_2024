<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Liaison_recipe_ustensil extends Model
{
    use HasFactory;

    protected $fillable = [
        'recipe_id','ustensil_id'
    ];

    public function ustensil(){
        return $this->belongsTo(Ustensil::class, 'ustensil_id');
    }

    public function recipeUstensil(){
        return $this->belongsTo(Recipe::class, 'recipe_id');
    }
}
