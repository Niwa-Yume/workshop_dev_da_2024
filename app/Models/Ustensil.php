<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ustensil extends Model
{
    use HasFactory;
    protected $table = 'ustensils';

    protected $fillable = [
        'name','image'
    ];


    public function liaison_recipes_ustensils(){
        return $this->hasMany(Liaison_recipe_ustensil::class);
    }
}
