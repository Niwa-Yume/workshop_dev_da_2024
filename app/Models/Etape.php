<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etape extends Model
{
    use HasFactory;

    protected $fillable = [
        'titre','instruction','ordre','besoin_code_parental','recipe_id','fichier'
    ];

    public function etapeRecipe(){
        return $this->belongsTo(Recipe::class, 'recipe_id');
    }
}
