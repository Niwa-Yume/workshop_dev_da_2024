<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    protected $table = 'types_unites';

    protected $fillable = [
        'name'
    ];


    public function IngredientUnite(){
        return $this->hasMany(Ingredient::class);
    }
}
