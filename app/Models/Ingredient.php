<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;

    protected $fillable = [
        'quantity','recipe_id','product_id','type_unite_id'
    ];

    public function recipeIngredient(){
        return $this->belongsTo(Recipe::class, 'recipe_id');
    }

    public function productIngredient(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function uniteIngredient(){
        return $this->belongsTo(Unit::class, 'type_unite_id');
    }
}
