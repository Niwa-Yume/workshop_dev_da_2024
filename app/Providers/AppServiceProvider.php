<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use App\Models\Enfant;
use Illuminate\Support\Facades\Event;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {

        if (config('app.env') === 'production') {
            \URL::forceScheme('https');
        }

        Event::listen(Registered::class, function ($event) {
            $user = $event->user;

            // Vérifie si le prénom de l'enfant est renseigné
            if ($user->prenom_enfant) {
                // Crée un nouvel enfant associé à l'utilisateur
                $enfant = new Enfant();
                $enfant->name = $user->prenom_enfant;
                $enfant->user_id = $user->id;

                // Définir le champ avatar_id sur 1
                $enfant->avatar_id = 1;
                $enfant->save();
            }
        });
    }
}
